//*************************************************************************************************/
//              JQUERY
//              ALEA ROOM
//
//*************************************************************************************************/

var NameTable = ['BenjaminF','YvannB','AlanG','KarineG','NoëmieL','Pierre-YvesM','JeromeM','ConstantinR','NicolasR','VincentS','Jean-RodrigueS','ThomasT','EricG','EricC','ThierryF','OlivierR','PerleH','NicolasB','NicolasP','DilanE','MathieuxB','DenisQ','KevinD','RominQ','GUEST'];

var FlagTable=new Array();
var str;
const CST_NbName = 25;
function ChooseName() {
    var idx = parseInt(Math.random() * CST_NbName);
    if (FlagTable[idx] == 1) {
        for (i = 0; i < CST_NbName; i++) {
            idx++;
            if (idx > (CST_NbName - 1))
                idx = 0;

            if (FlagTable[idx] == 0)
                break;
        }
    }
    FlagTable[idx] = 1;
    return (idx);
}

$('#but').click(function () {
    var tmpidx = 0;

    //------ Reinit table des flag
    for (i = 0; i < CST_NbName; i++) {
        FlagTable[i] = 0;
    }

    //------ Choose name
    for (jj = 0; jj < 5; jj++) {
        for (ii = 0; ii < 5; ii++) {
            tmpidx = ChooseName();
            $($('div p')[ii + (jj * 5)]).html(NameTable[tmpidx]);
        }
    }
});
